import * as React from 'react';
import Livro, { ILivros } from './Livro'
import { inject, observer } from 'mobx-react';
import Store from './store/Store';
import { createStyles } from '@material-ui/core';

const Style = createStyles({
  styleContainer: {
    marginLeft: 220,
    display: 'flex',
    flexWrap: 'wrap',
    marginTop: 100,
    padding: 20
  }  
})

interface InnerProps {
  StoreApp: Store
}

@inject("StoreApp")
@observer
class ContainerLivros extends React.Component<InnerProps, {}> {

  public render() {

    const itensLivros = this.props.StoreApp.DadosBiblioteca.map((livroRender: ILivros, i: number) => {
      return (
        <div key={i} >
          <Livro
            // Envia propriedades do objeto livroRender para o componente livro
            {...livroRender}
           />
        </div>
      )
    });

    return (
      <div style={Style.styleContainer}>
        {itensLivros}
      </div>
    );
  }

}

export default ContainerLivros as React.ComponentType<{}>;