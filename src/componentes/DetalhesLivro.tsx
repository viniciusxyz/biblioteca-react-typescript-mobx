import * as React from 'react'
import { Paper, Typography, Button, Icon, createStyles } from '@material-ui/core';
import AddShoppingCart from '@material-ui/icons/AddShoppingCart'
import RemoveShoppingCart from '@material-ui/icons/RemoveShoppingCart'
import { ILivros } from './Livro';
import { inject, observer } from 'mobx-react';
import Store from './store/Store';

const Style = createStyles({
    StyleContainerDetalhes: {
        padding: 15,
        maxHeight: '100%',
        minWidth: 300
    },
    
    StyleImagemLivro: {
        margin: 20
    },
    
    StyleTituloLivro: {
        marginLeft: 220,
        marginTop: 40,
        position: 'absolute',
        fontSize: 20,
    },
    StyleQtdCompra: {
        padding: 10,
        backgroundColor: '#fff18a'
    }

})

interface InnerProps {
    StoreApp: Store
}

@inject("StoreApp")
@observer
class DetalhesLivro extends React.Component<ILivros & InnerProps>{

    public imgLivro = require('../assets/images/' + this.props.Imagem)

    public StyleTextButton: React.CSSProperties = {
        fontSize: 16,
        float: 'right',
        marginLeft: 20
    }

    public StyleButton: React.CSSProperties = {
        backgroundColor: '#adb7ff',
        marginTop: 10
    }

    public render() {
        // Cria um array para retornar todos os livros vendidos com o titulo do livro deste componente
        const ArrayVendidos: Array<ILivros> = this.props.StoreApp.ArrayLivrosCarrinho.filter((item: ILivros, index: number, ArrayOriginal: ILivros[]) => {
            return item.TituloLivro === this.props.TituloLivro
        })

        const QuantidadeVendidos = ArrayVendidos.length

        return (

            <Paper style={Style.StyleContainerDetalhes}>
                <Paper style={{...Style.StyleQtdCompra, display: QuantidadeVendidos > 0 ? 'table': 'none'}}>
                    {QuantidadeVendidos} no Carrinho
                </Paper>
                <Typography
                    style={Style.StyleTituloLivro}
                >
                    {this.props.TituloLivro}
                </Typography>
                <img
                    style={Style.StyleImagemLivro}
                    src={this.imgLivro}
                    title={this.props.TituloLivro}
                />
                <Typography
                    variant="subheading"
                >
                    {this.props.Conteudo}
                </Typography>
                <Button
                    onClick={this.props.StoreApp.AddItemCarrinho(this.props)}
                    variant='contained'
                    fullWidth={true}
                    name="Add Livro"
                    style={this.StyleButton}
                >
                    <Icon color="primary">
                        <AddShoppingCart />
                    </Icon>
                    <Typography style={this.StyleTextButton}>
                        R$ {this.props.Valor}
                    </ Typography>
                </Button>
                <Button
                    onClick={this.props.StoreApp.RemoveItemCarrinho(this.props)}
                    variant='contained'
                    fullWidth={true}
                    name="Remove"
                    style={{...this.StyleButton,display: QuantidadeVendidos > 0 ? 'flex': 'none'}}
                >
                    <Icon color="primary">
                        <RemoveShoppingCart />
                    </Icon>
                    <Typography style={this.StyleTextButton}>
                        Retirar do carrinho
                    </ Typography>
                </Button>
            </Paper>
        )
    }
}

export default DetalhesLivro as React.ComponentType<ILivros>