import * as React from 'react'
import { observer, inject } from 'mobx-react';
import { AppBar, Toolbar, Typography, Dialog, ButtonBase, createStyles } from '@material-ui/core';
import LiberLogo from '../assets/images/LiberLogo.png'
import * as ReactAutocomplete from 'react-autocomplete'
import DadosBiblioteca from '../assets/json/DadosBiblioteca.json'
import DetalhesLivro from './DetalhesLivro';
import Store from './store/Store';
import { ILivros } from './Livro';

const Style = createStyles({
    styleHeader: {
        height: 100,
    },

    LogoImg: {
        width: 75,
        marginLeft: 15,
        padding: 10,

    },

    SearchBar: {
        backgroundColor: 'white',
        borderRadius: 5,
        width: '100%',
        margin: '0px auto',
        padding: 8,
        border: 'none'
    },

    StyleTituloSite: {
        marginLeft: 15

    },

    StyleContainerSearchBar: {
        width: '70%',
        marginLeft: 23,
        border: 'none',
    },

    StyleImgBusca: {
        width: 70,
        float: 'left'
    },

    TituloLivroSearch: {
        margin: 30,
        float: 'left'
    },

    StyleAutoCompleteBar: {
        display: 'grid',

        borderRadius: 3,
        boxShadow: 'rgba(0, 0, 0, 0.1) 0px 2px 12px',
        background: 'rgba(255, 255, 255, 0.9)',
        padding: '2 0',
        fontSize: '90%',
        position: 'fixed',
        overflow: 'auto',
        maxHeight: '50%',
        left: 198.703,
        top: 46.5,
        minWidth: '68%',
    }
})

interface InnerProps {
    StoreApp: Store
}

interface InnerState {
    DisplayMode: string
    value: string
    DialogOpened: boolean
    LivroDialog: ILivros

}

@inject("StoreApp")
@observer
class HeaderBiblioteca extends React.Component<InnerProps, InnerState>
{
    public state = {
        DisplayMode: 'none',
        value: '',
        DialogOpened: false,

        // Inicia estado com objeto Livro Vázio
        LivroDialog: {
            TituloLivro: '',
            Conteudo: '',
            Imagem: '',
            Valor: '',
            Autor: '',
            Genero: ''
        }
    }


    public FiltroRender = (item: ILivros, value: string) => {

        // Caso o trecho digitado exista dentro de um dos titulos o mesmo retornorá true
        return item.TituloLivro.toLowerCase().indexOf(value.toLowerCase()) > -1
    }

    public handleOpenDialog = (Livro: ILivros) => () => {
        this.setState(
            {
                LivroDialog: Livro,
                DialogOpened: true
            }
        )
    }

    public handleCloseDialog = () => {
        this.setState({ DialogOpened: false })
    }

    public handleChangeReactAutocomplete = () => (event: React.ChangeEvent<any>) => {
        this.setState({
            value: event.target.value
        })
    }

    public renderItens = (Livro: ILivros, highlighted: boolean) => {
        const imgLivro = require("../assets/images/" + Livro.Imagem)
        return (
            <ButtonBase
                // tslint:disable-next-line:jsx-no-lambda
                onClickCapture={this.handleOpenDialog(Livro)}
                key={Livro.TituloLivro}
                style={{ backgroundColor: highlighted ? '#fff' : 'transparent', width: '100%', color: '#000', display: 'inline-block' }}
            >
                <img src={imgLivro} title={Livro.TituloLivro} style={Style.StyleImgBusca} />
                <Typography
                    variant="title"
                    noWrap={true}
                    style={Style.TituloLivroSearch}
                >
                    {Livro.TituloLivro}
                </Typography>
            </ButtonBase>
        )
    }

    public render() {
        return (
            <AppBar position="fixed">
                <Toolbar>

                    <img src={LiberLogo} alt="Logo Liber" style={Style.LogoImg} /> {/*Logo*/}

                    <Typography
                        variant="title"
                        color="inherit"
                        style={Style.StyleTituloSite}
                    >
                        Liber

                     </Typography>
                    <ReactAutocomplete
                        menuStyle={Style.StyleAutoCompleteBar}
                        wrapperStyle={Style.StyleContainerSearchBar}
                        items={DadosBiblioteca.livros}
                        inputProps={{ style: Style.SearchBar }}
                        shouldItemRender={this.FiltroRender}
                        // tslint:disable-next-line:jsx-no-lambda
                        getItemValue={(item: ILivros) => item.TituloLivro}
                        renderItem={this.renderItens}
                        value={this.state.value}
                        onChange={this.handleChangeReactAutocomplete()}
                    />
                </Toolbar>
                <Dialog
                    open={this.state.DialogOpened}
                    onClose={this.handleCloseDialog}
                >
                    <DetalhesLivro
                        {...this.state.LivroDialog}
                    />
                </Dialog>

            </AppBar>
        )
    }
}

export default HeaderBiblioteca as React.ComponentType<{}>;
